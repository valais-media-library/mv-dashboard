<h1 align="center">MV-Dashboard</h1> <br>
<!--<p align="center">
    <img alt="mv-udc logo" title="mv-dashboard logo" src="#" width="300">
</p>-->
<div align="center">
  <strong>An app to rule them all</strong>
</div>
<div align="center">
  Dashboard for the Valais Media Library for the library staff.
</div>

<div align="center">
  <h3>
    <a href="https://valais-media-library.gitlab.io/mv-dashboard/">Access</a>
    <span> | </span>
    <a href="#documentation">Documentation</a>
    <span> | </span>
    <a href="#contributing">
      Contributing
    </a>
  </h3>
</div>

<div align="center">
  <sub>Built with ❤︎ in Valais by <a href="https://gitlab.com/valais-media-library/mv-dashboard/-/graphs/master">contributors</a>
</div>


## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Usage](#usage)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [History and changelog](#history-and-changelog)
- [Authors and acknowledgment](#authors-and-acknowledgment)

## Introduction

[![license](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square)](https://gitlab.com/valais-media-library/mv-dashboard/blob/master/LICENSE)
[![pipeline status](https://gitlab.com/valais-media-library/mv-dashboard/badges/master/pipeline.svg)](https://gitlab.com/valais-media-library/mv-dashboard/commits/master)


👉 [View online](https://valais-media-library.gitlab.io/mv-dashboard/)

## Features

- Browse applications by sites and uses
- links to the aplications

## Documentation

To modify, add or remove content of the dashboard, you have to modify the `db.json`. Pay attention to the syntax. If in doubt, use a [validator](https://jsonlint.com/).

### Installation

[Download the zip file](https://gitlab.com/valais-media-library/mv-dashboard/-/archive/master/mv-dashboard-master.zip) from the latest release and unzip it somewhere reachable by your webserver.

### GitLab CI

This project's static Pages are built by [GitLab CI](https://about.gitlab.com/gitlab-ci/) every time you push or update the repository, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml). It expects to put all your HTML files in the `public/` directory.

## Contributing

We’re really happy to accept contributions from the community, that’s the main reason why we open-sourced it! There are many ways to contribute, even if you’re not a technical person. Check the [roadmap](#roadmap) or [create an issue](https://gitlab.com/valais-media-library/mv-udc/issues) if you have a question.

1. [Fork](https://help.github.com/articles/fork-a-repo/) this [project](https://gitlab.com/valais-media-library/mv-dashboard/)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

## History and changelog

You will find changelogs and releases in the [CHANGELOG](https://gitlab.com/valais-media-library/mv-dashboard/blob/master/CHANGELOG) file.

## Roadmap

Nothing

## Authors and acknowledgment

* **Michael Ravedoni** - *Initial work* - [michaelravedoni](https://gitlab.com/michaelravedoni)

See also the list of [contributors](https://gitlab.com/valais-media-library/mv-dashboard/-/graphs/master) who participated in this project.

* [Vue.js](https://github.com/vuejs/vue) Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.

## License

[MIT License](https://gitlab.com/valais-media-library/mv-dashboard/blob/master/LICENSE)
